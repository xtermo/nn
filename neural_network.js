"use strict";															// Require javascript to run in strict mode.
const NEURON = require( './neuron.js' );	// Use Node.js to import the neuron module.
const FILESYSTEM = require( 'fs' );				// Use Node.js to import the fs module.

module.exports = function ( size, handler ) {
	this.cube_size      					// The length of one side of the cube of neurons.
																// Must be an odd integer for uniform distribution of inhibitory neurons.
												= ( size % 2 === 1 ) ? size : +( size + 1 );
	this.neurons 					= [];			// The list of neurons connected to this network.
	this.neuron_updaters	= [];			// A list of handles directly to each neuron's updater.
	this.axon_radius 								// How far away two neurons can be from each other and still signal one another.
												= size / 4;
	this.input_stream 		= [];			// The list of input values to go to the input neurons.
	this.input_neurons 		= [];			// The subset of the neurons to be treated as input-only.
	this.output_neurons		= [];			// The subset of the neurons ot be treated as output-only.
	this.neuron_firings 	= [];			// The list of which neurons have fired during the previous tick.
	this.result_handler							// The output handler to evaluate each tick's output.
												= handler;
	this.dopamine_level 	= 1.0;		// A multiplier indicating the overall rate of learning.
	this.input_stream			= [];			// The list of inputs to be processed in order.
	this.will_stop 				= false;	// A flag indicating whether the network should halt after the current tick.

	// Runs a function once, then destroys it.
	this.run_once = ( function_to_run, argument_list = [] ) => {
		return new Promise( ( resolve, reject ) => {
			if ( typeof function_to_run === 'undefined' || !Reflect.has( this, function_to_run ) ) {
				return reject( 'Function not available to be run once.' );
			}
			Reflect.apply( this[ function_to_run ], this, argument_list )
			.catch( ( rejection ) => {
				return reject( `Could not perform run-once. Details:\n ${rejection}` );
			} )
			.then( () => {
				Reflect.deleteProperty( this, function_to_run );
			} )
			.catch( ( rejection_details ) => {
				return reject( `Could not delete. Details:\n ${rejection_details}` );
			} )
			.then( () => {
				return resolve( true );
			} );
		} );
	};

	// Gets the Euclidean distance between 2 neurons in this neural network.
	this.get_neuron_distance = ( neuron_a, neuron_b ) => {
		return new Promise( ( resolve, reject ) => {
			if ( !Array.isArray( neuron_a )
			|| !Array.isArray( neuron_b )
			|| neuron_a.length !== 3
			|| neuron_b.length !== 3 ) {
				return reject( `An invalid neuron location was input. ${neuron_a}, ${neuron_b}` );
			}
			let distance
			= Math.sqrt(
				( Math.pow( neuron_a[ 0 ] - neuron_b[ 0 ], 2.0 ) )
				+ ( Math.pow( neuron_a[ 1 ] - neuron_b[ 1 ], 2.0 ) )
				+ ( Math.pow( neuron_a[ 2 ] - neuron_b[ 2 ], 2.0 ) ) );
			return resolve( distance );
		} );
	};

	// Sets up this neural network to run.
	this._initialize = ( input_neurons ) => {
		return new Promise( ( resolve, reject ) => {
			// Generate the neurons to use if none were input.
			if ( !Array.isArray( input_neurons ) || !( input_neurons.length > 1 )  ) {
				this.generate_neurons()
				.then( ( new_neurons ) => {
					this.neurons = new_neurons.slice();
				} )
				.catch( ( neurons_rejection ) => {
					return reject( `Could not generate neurons. Details:\n ${neurons_rejection}` );
				} )
				.then( () => {
					return this.find_all_neuron_neighbors();
				} )
				.catch( ( neighbor_finding_rejection ) => {
					return reject( `Could not find all neighbors. Details:\n ${neighbor_finding_rejection}` );
				} )
				.then( ( neighbors_results ) => {
					let neurons_length = this.neurons.length;
					for ( let current_neuron = 0; current_neuron < neurons_length; current_neuron++ ) {
						this.neuron_updaters[ current_neuron ] = this.neurons[ current_neuron ].update;
						this.neurons[ current_neuron ].run_once( '_initialize', [ {
							neighbors: neighbors_results[ current_neuron ],
							cube_size: this.cube_size,
						} ] )
						.catch( ( neuron_initialization_rejection ) => {
							return reject( `Neuron could not be initialized. Details:\n ${neuron_initialization_rejection}` );
						} );
					}
					return resolve( true );
				} );
			} else {
				this.neurons = input_neurons.slice();
			}
		} );
	};

	// Generates a cube of neurons with an input and output slice.
	this.generate_neurons = () => {
		console.log( 'Generating new neurons...' );
		return new Promise( ( resolve ) => {
			// The below setup uses slightly more RAM than is absolutely needed, but should perform its logic comparatively quickly.
			// This process should only happen once, so the RAM it uses should become freed for later use anyway.
			let neuron_data 	= [];
			let inputs 				= [];
			let inputs_count	= 0;
			let outputs 			= [];
			let outputs_count	= 0;
			let master_count	= 0;
			let cube_size			= this.cube_size;
			for (  let x_position = 0; x_position < cube_size; x_position++ ) {
				for ( let y_position = 0; y_position < cube_size; y_position++ ) {
					for ( let z_position = 0; z_position < cube_size; z_position++ ) {
						// By default, neurons are used for throughput.
						let job = 'throughput';
						// Changes the type if this neuron is on the face of a network, but not an edge or corner.
						// Inputs have exactly one minimum x, y, or z position and outputs have exactly one maximum x, y, or z position.
						// This omission prevents overlapping or directly-touching input and output neurons.
						let cube_last = cube_size - 1;
						let has_min_x = x_position === 0;
						let has_min_y = y_position === 0;
						let has_min_z = z_position === 0;
						let has_max_x = x_position === cube_last;
						let has_max_y = y_position === cube_last;
						let has_max_z = z_position === cube_last;
						let no_min_x = !has_min_x;
						let no_min_y = !has_min_y;
						let no_min_z = !has_min_z;
						let no_max_x = !has_max_x;
						let no_max_y = !has_max_y;
						let no_max_z = !has_max_z;

						if ( ( has_min_x && no_min_y && no_min_z && no_max_y && no_max_z )
						|| ( has_min_y && no_min_x && no_min_z && no_max_x && no_max_z )
						|| ( has_min_z && no_min_y && no_min_x && no_max_y && no_max_x ) ) {
							job = 'input';
							inputs[ inputs_count ] = master_count;
							inputs_count += 1;
						} else if ( ( has_max_x && no_max_y && no_max_z && no_min_y && no_min_z )
						|| ( has_max_y && no_max_x && no_max_z && no_min_x && no_min_z )
						|| ( has_max_z && no_max_y && no_max_x && no_min_y && no_min_x ) ) {
							job = 'output';
							outputs[ outputs_count ] = master_count;
							outputs_count += 1;
						}
						// By default, neurons are excitatory.
						let type = 1.0;
						// Even-numbered throughput neurons are inhibitory.
						if ( job === 'throughput' && master_count % 2 === 0 ) {
							type = -1.0;
						}
						neuron_data[ master_count ]
						= new NEURON( job, type, [ x_position, y_position, z_position ] );
						this.neuron_firings[ master_count ] = 0.0;
						master_count += 1;
					}
				}
			}
			this.input_neurons = inputs.slice();
			this.output_neurons = outputs.slice();
			return resolve( neuron_data );
		} );
	};

	// Generates a list of neurons within the axon radius of the input coordinates.
	this.find_neuron_neighbors = ( neuron_coordinates ) => {
		return new Promise( ( resolve, reject ) => {
			if ( this.neurons.length < 1 ) {
				return reject( 'No neurons available yet.' );
			}
			if ( !Array.isArray( neuron_coordinates ) || neuron_coordinates.length !== 3 ) {
				return reject( `Invalid input neuron specified: ${neuron_coordinates}` );
			}
			let results 				= [];
			let result_count		= 0;
			let neurons_length	= this.neurons.length;
			let radius					= this.axon_radius;
			for ( let current_neuron = 0; current_neuron < neurons_length; current_neuron++ ) {
				if ( this.neurons[ 0 ] === neuron_coordinates[ 0 ]
				&& this.neurons[ 0 ] === neuron_coordinates[ 0 ]
				&& this.neurons[ 0 ] === neuron_coordinates[ 0 ] ) {
					// This is the same neuron. Don't a neuron in its own neighbors list.
					continue;
				}
				this.get_neuron_distance( neuron_coordinates, this.neurons[ current_neuron ].position )
				.then( ( distance ) => {
					if ( !( radius <  distance ) ) {
						results[ result_count ] = current_neuron;
						result_count += 1;
					}
				} );
			}
			return resolve( results );
		} );
	};

	// Finds and populates the neighbors of every neuron in this neural network.
	this.find_all_neuron_neighbors = () => {
		return new Promise( ( resolve, reject ) => {
			console.log( 'Finding all valid synapses...' );
			if ( !Array.isArray( this.neurons ) || this.neurons.length < 1 ) {
				return reject( 'No neurons available to find neighbors for yet.' );
			}
			let all_neighbors = [];
			let neurons_length = this.neurons.length;
			for ( let current_neuron = 0; current_neuron < neurons_length; current_neuron++ ) {
				if ( this.neurons[ current_neuron ].job === 'input' ) {
					// Inputs don't get their data from neighbors.
					// Instead, their values are assigned directly from an external source.
					// Don't bother to find their neighbors as this is time-consuming.
					continue;
				}
				this.find_neuron_neighbors( this.neurons[ current_neuron ].position )
				.then( ( neighbor_result ) => {
					all_neighbors[ current_neuron ] = neighbor_result.slice();
				} );
			}
			return resolve( all_neighbors );
		} );
	};

	// Runs all neurons' updaters asynchronously. Returns the resulting firings.
	this.update_all_neurons = ( updaters ) => {
		return new Promise( ( resolve, reject ) => {
			let firings = this.neuron_firings.slice();
			// Makes the results of the updaters asynchronous and iterable.
			let update_generator = function *( updaters_list, firings_list ) {
				// Makes the list of updaters iterable.
				let selector = updaters_list[ Symbol.iterator ]();
				let length = updaters_list.length;
				for ( let current_neuron = 0; current_neuron < length; current_neuron++ ) {
					yield selector.next().value( firings_list );
					// After each yield, the next call will continue from here.
				}
			};
			// Asynchronously call all of the updaters. Advance only if all resolve.
			Promise.all( update_generator( updaters, firings ) )
			.catch( ( generator_rejection ) => {
				return reject( `Could not update neurons. Details:\n ${generator_rejection}` );
			} )
			.then( ( firing_results ) => {
				// Return with the in-order list of asynchronously-generated firings.
				return resolve( firing_results );
			} );
		} );
	};

	// Advances to the next tick and checks all neurons.
	// Inputs get hard-sent as firings on one-way neurons in the neuron set.
	this.advance_tick = ( inputs ) => {
		// console.log( '********   NEW TICK   ********' );
		return new Promise( ( resolve, reject ) => {
			if ( typeof inputs === 'undefined' ) {
				return reject( 'No input available for this tick.' );
			}
			// Apply input "firings".
			let inputs_length = inputs.length;
			for ( let current_input = 0; current_input < inputs_length; current_input++ ) {
				// Loads inputs directly into firing data for this tick to process.
				this.neuron_firings[ this.input_neurons[ current_input ] ] = inputs[ current_input ];
			}
			let updaters = this.neuron_updaters;
			this.update_all_neurons( updaters )
			.catch( ( updater_rejection ) => {
				return reject( `Neuron update for this tick failed. Details:\n ${updater_rejection}` );
			} )
			.then( ( firing_results ) => {
				this.neuron_firings = firing_results.slice();
			} )
			.then( this.process_results )
			// Process the results to see if they contain a desirable outcome. If so, enhance reinforcement for this tick.
			.catch( ( result_check_rejection ) => {
				return reject( `Firing results could not be processed. Details:\n ${result_check_rejection}` );
			} )
			.then( ( results ) => {
				if ( results ) {
					for ( let current_neuron in this.neurons ) {
						this.neurons[ current_neuron ].reinforce( 1.05 ); // This reinforcement is not as strong as the initial reinforcement, but compounts with it.
					}
				}
			} )
			.catch( ( update_rejection ) => {
				return reject( `Neurons could not be reinforced. Details:\n ${update_rejection}` );
			} )
			.then( () => {
				return resolve( inputs );
			} );
		} );
	};

	// Use an external handler to determine whether the results yeilded warrant further reinforcement.
	this.process_results = () => {
		return new Promise( ( resolve, reject ) => {
			if ( typeof this.result_handler === 'function' ) {
				Reflect.apply( this.result_handler, this, [ this.neuron_firings, this.output_neurons ] )
				.catch( ( rejection_from_handler ) => {
					return reject( `Could not check output results. Details:\n ${rejection_from_handler}` );
				} )
				.then( ( result_from_handler ) => {
					return resolve( result_from_handler );
				} );
			} else {
				// No result success could be determined, but nothing broke either.
				return resolve( 'false' );
			}
		} );
	};

	// Saves the current network as a JSON string.
	this.save = ( file_name ) => {
		return new Promise( ( resolve, reject ) => {
			if ( typeof file_name !== 'string' ) {
				return reject( 'No file name provided.' );
			}
			let save_file_contents = JSON.stringify( {
				cube_size: this.cube_size,
				axon_radius: this.axon_radius,
				neurons: this.neurons,
				input_neurons: this.input_neurons,
				output_neurons: this.output_neurons,
				neuron_firings: this.neuron_firings,
				dopamine_level: this.dopamine_level,
			} );
			FILESYSTEM.writeFile( `./saved/${file_name}.json`, save_file_contents );
			return resolve( true );
		} );
	};

	// Synchronizes each tick by queueing a check of whether or not to continue only after a tick has finished.
	this.continue = () => {
		this.advance_tick( this.input_stream.shift() )
		.then( this.check_if_done );
	};

	// Stops the network from running continuously if the input stream is empty. Otherwise, continues.
	this.check_if_done = () => {
		if ( this.input_stream.length > 0 ) {
			this.continue();
		} else {
			console.log( 'All inputs processed. Exiting automatic processing mode...' );
			return Promise.resolve( true );
		}
	};

	// Continuously calls advanceTick until the stop flag is set.
	this.start = ( input_stream ) => {
		return new Promise( ( resolve, reject ) => {
			if ( !Array.isArray( input_stream ) ) {
				return reject( 'No  input provided.' );
			}
			this.input_stream = input_stream.slice();
			this.will_stop = false;
			this.check_if_done();
			return resolve( true );
		} );
	};

	// Pushes a new input set onto the input stream.
	this.push_input = ( new_input ) => {
		return new Promise( ( resolve, reject ) => {
			if ( !Array.isArray( new_input ) ) {
				return reject( `Could not push a new input to the stream because it is not formatted properly.` );
			}
			this.input_stream[ this.input_stream.length ] = new_input.slice();
			return resolve( true );
		} );

	};

	// Sets the stop flag.
	this.stop = () => {
		return new Promise( ( resolve ) => {
			this.will_stop = true;
			return resolve( true );
		} );
	};
};
