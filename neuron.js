"use strict"; // Require javascript to run in strict mode.
const REST_THRESHOLD = 100.0;
const REST_POTENTIAL = 0.0;

module.exports = function ( job, type, position ) {
	this.job 								= job;			// Defines whether this neuron is for input, throughput, or output.
	this.position 					= position;	// The X, Y, and Z coordinates of this neuron.
																			// Used by the neural network to calculate this neuron's neighbors.
	this.potential					= 0.0; 			// Current cumulative charge.
	this.threshold											// Charge required to fire.
													= REST_THRESHOLD;
	this.fire_type					= type; 		// Sets whether this neuron is inhibitive or activative. May be 1.0 or -1.0.
	this.firing							= 0.0; 			// Sets whether this neuron is firing for this tick. May be -1.0, 0.0, or 1.0.
	this.neighbors					= [];       // Indexes of this neuron's neighbors.
																			// This is determined by the neural network and the axon radius of this neuron.
	this.neighbor_weights		= []; 			// The weight attributed to all neighbors' firings.
	this.reinforcement_rate	= 0.5; 			// The rate at which reinforcment is allowed to occur.
	this.cube_size					= 1.0;			// The size of the neural network on one side.

	// Runs a function once, then destroys it.
	this.run_once = ( function_to_run, argument_list ) => {
		return new Promise( ( resolve, reject ) => {
			if ( !Reflect.has( this, function_to_run ) ) {
				return reject( 'Function not available to be run once in this neuron.' );
			}
			Reflect.apply( this[ function_to_run ], this, argument_list )
			.catch( ( rejection ) => {
				return reject( `Could not perform run-once in this neuron. Details:\n ${rejection}` );
			} )
			.then( () => {
				Reflect.deleteProperty( this, function_to_run );
				return;
			} )
			.catch( ( rejection_details ) => {
				return reject( `Could not delete in this neuron. Details:\n ${rejection_details}` );
			} )
			.then( () => {
				return resolve( true );
			} );
		} );
	};

	// Sets up a neuron.
	this._initialize = ( input ) => {
		return new Promise( ( resolve, reject ) => {
			if ( typeof input === 'undefined' ) {
				return reject( 'No input data available for neuron initialization.' );
			}
			this.cube_size = ( typeof input.cube_size !== 'undefined' ) ? input.cube_size : 1.0;
			this.neighbors = ( typeof input.neighbors !== 'undefined' ) ? input.neighbors.slice() : [];
			this.potential = ( typeof input.potential !== 'undefined' ) ? input.potential : this.potential; // allows for override later.
			this.threshold = ( typeof input.threshold !== 'undefined' ) ? input.threshold : this.threshold; // allows for override later.
			let neighbors_length = ( Array.isArray( input.neighbors ) ) ? input.neighbors.length : 0;
			for ( let current_neighbor = 0; current_neighbor < neighbors_length; current_neighbor++ ) {
				this.neighbor_weights[ current_neighbor ] = REST_THRESHOLD * ( this.cube_size ) / neighbors_length;
			}
			return resolve( this );
		} );
	};

	// Checks neighbor firings to update this neuron for this tick.
	this.update = ( presynaptic_neuron_firings ) => {
		return new Promise( ( resolve, reject ) => {
			if ( !Array.isArray( presynaptic_neuron_firings ) ) {
				return reject( 'Could not update neuron. No firing data was available.' );
			}
			if ( this.job === 'input' ) {
				// Inputs don't check their neighbors' firings to determine if they've fired.
				// Their firing results will be determined and applied externally.
				return resolve( 0.0 );
			}
			// Set firing flag to false.
			this.firing = 0.0;
			// Decay.
			Promise.resolve()
			.then( () => {
				let masked = [];
				let sum = 0.0;
				// Mask the neighbor weights list with the last entry in the firings list.
				let neighbor_length = this.neighbors.length;
				for ( let index = 0; index < neighbor_length; index++ ) {
					masked[ index ] = this.neighbor_weights[ index ] * presynaptic_neuron_firings[ this.neighbors[ index ] ];
					sum += masked[ index ];
				}
				this.potential += sum;
				if ( this.potential < 0 ) {
					this.potential = 0;
				}
				//console.log( `Potential: ${this.potential}\n` );
				//console.log( `threshold: ${this.threshold}\n` );
				// Check if this neuron fired.
				if ( this.potential > this.threshold ) {
					//console.log( `Fired.` );

					// Raise threshold.
					this.threshold *= this.cube_size / 2; // This seems to keep neurons from ever firing.
					// Reset potential.
					this.potential = 0.0;
					// Set firing flag.
					this.firing = 1.0 * this.fire_type;
				}
				this.reinforce( 1.1, presynaptic_neuron_firings )
				.catch( ( reinforce_rejection ) => {
					return reject( `Could not reinforce. Details: ${reinforce_rejection}` );
				} )
				.then( () => {
					this.decay()
					.then( true );
				} )
				.catch( ( decay_rejection ) => {
					return reject( `Could not perform decay on this neuron. Details:\n ${decay_rejection}` );
				} )
				.then( () => {
					return resolve( this.firing );
				} );
			} );
		} );
	};

	// Modulates the rate of reinforcement of neighbor relationships for this tick.
	this.reinforce = ( reinforcement_multiplier, presynaptic_neuron_firings ) => {
		return new Promise( ( resolve, reject ) => {
			if ( typeof reinforcement_multiplier !== 'number' || reinforcement_multiplier < 1.0 ) {
				return reject( `Invalid reinforcement_multiplier ${reinforcement_multiplier}.` );
			}
			if( !Array.isArray( presynaptic_neuron_firings ) ) {
				return reject( `Invalid neighbor firings list.` );
			}
			let strength = 1.0; // The default weight applied to all fired neurons. Prevents decay, but does not reinforce.
			if ( this.fired ) {
				strength = 1.1 * reinforcement_multiplier;
			} else if ( this.potential / this.threshold > 0.667 ) {
				strength = 1.05 * reinforcement_multiplier;
			}
			let firing_length = presynaptic_neuron_firings.length;
			for ( let current_neighbor = 0; current_neighbor < firing_length; current_neighbor++ ) {
				if ( presynaptic_neuron_firings[ current_neighbor ] ) {
					// Applies the reinforcement to this weight.
					this.neighbor_weights[ current_neighbor ] *= strength;
					// Negates the decay on this weight in the next tick.
					this.neighbor_weights[ current_neighbor ] += 0.1;
					if ( this.neighbor_weights[ current_neighbor ] > 1000.0 ) {
						this.neighbor_weights[ current_neighbor ] = 1000.0;
					}
				}
			}
			return resolve( true );
		} );
	};

	// Cause all temporarily-changed values to decay toward their natrual resting point.
	this.decay = () => {
		return new Promise( ( resolve ) => {
			// Decays the potential toward 0 on each tick.
			this.potential += ( REST_POTENTIAL - this.potential ) / this.cube_size ;
			// Decays the threshold towards 100 on each tick.
			this.threshold += ( REST_THRESHOLD - this.threshold ) / 2.0;
			// Decays all neighbor weights by a tiny amount.
			let neighbor_weights_length = this.neighbor_weights.length;
			for ( let current_neighbor = 0; current_neighbor < neighbor_weights_length; current_neighbor++ ) {
				this.neighbor_weights[ current_neighbor ] -= 0.1 / neighbor_weights_length ;
				if( this.neighbor_weights[ current_neighbor ] < 0.0 ) {
					this.neighbor_weights[ current_neighbor ] = 0.0;
				}
			}
			return resolve( true );
		} );
	};
};
