let arr = [];
let arrmeth = []
let Obj = function( num, arg ) {
	this.number = num;
	this.doStuff = ( arg ) => {
		return new Promise( (resolve) => {
			this.number += arg;
			return resolve( this.number );
		});
	};
};

for (let i = 0; i < 10; i++) {
	arr[ i ] = new Obj(i, (2 * i ));
	arrmeth[ i ] = arr[ i ].doStuff;
}

console.log(arr);

let gen = function *( array_in, args ) {

	//function* select ( array_in ) {
	//		yield* array_in;
	//};

	//let selector = select( array_in );

	let length = array_in.length;
	let selector = array_in[Symbol.iterator]();
	for (let i = 0; i < length; i++)
	{
		let selected = selector.next().value.doStuff( args[i] );
		//console.log(selected);
		yield selected;
	}
};

let gentoo = gen(arr, [1,2,3,4,5,6,7,8,9,10]);

Promise.all(gentoo)
.catch( (rejection) => {
	console.log('rejection: ',rejection);
})
.then( (result) => {
	//console.log(arr);
	return result;
})
.then( (results2) => {
	console.log(results2);
	gentoo = gen(arr, [1,2,3,4,5,6,7,8,9,10]);
	Promise.all(gentoo)
	.catch( (rejection) => {
		//console.log('rejection: ',rejection);
	})
	.then( (result) => {
		//console.log(arr);
	});
});
