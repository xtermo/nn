/*
Neural Network Control System.
Meant to run with Node.js 6.0
*/
"use strict";																				// Require javascript to run in strict mode.
const NN 				= require( './neural_network.js' ); // Use Node.js to import the Neural Network module.
const COLORS		= require( 'colors' );							// Use Node.js to import the Colors module.

COLORS.setTheme( {
	one: 'yellow',
	zero: 'cyan',
	error: 'red',
} );

let result_handler = ( firings, outputs ) => {

	// console.log( 'Handler', firings );

	// /*
	let result = [];
	let result_length = 0;
	let firings_length = firings.length;
	for ( let index = 0; index < firings_length; index++ ) {
		let location = outputs.indexOf( index );
		if( location !== -1 ) {
			let precolor = firings[ outputs[ location ] ];
			if ( precolor ) {
				result[ result_length ] = precolor.toString().one;
			} else {
				result[ result_length ] = precolor.toString().zero;
			}
			result_length += 1;
		}
	}
	console.log( `Outputs: [ ${result.join()} ]` );
	// */
	return Promise.resolve( false );
};

let total_inputs	= 10000;
let cube_size			= 7;
let net1					= new NN( cube_size, result_handler );
let input_length = 3 * ( Math.pow( cube_size - 2, 2.0) );
let sample_input	= [];

for ( let current_input = 0; current_input < total_inputs; current_input++ ) {
	let temporary_inputs = [];
	for ( let current_input_element = 0; current_input_element < input_length; current_input_element++ ) {
		if ( current_input_element > -1 && current_input_element < 50 ) {
			temporary_inputs[ temporary_inputs.length ] = 1;
		} else {
			temporary_inputs[ temporary_inputs.length ] = 0;
		}
	}
	sample_input[ sample_input.length ] = temporary_inputs.slice();
}

console.log( sample_input.length );

net1.run_once( '_initialize' )
.catch( ( rejection ) => {
	console.error( 'ERROR:\n', rejection.error );
} )
.then( ( initialized ) => {
	if ( initialized ) {
		console.log( `Initialization complete. ${net1.input_neurons.length} inputs ready.` );
	}
} )
.then( () => {
	net1.start(sample_input);
} );
